import { h, Component } from "preact";
import Pop from "./Pop.jsx";
import pageMode from "../lib/api/pageMode";
import pixelpop from "../lib/api";

export default class App extends Component {
  constructor(props) {
    super(props);

    this.previousInstancesHash = "";
    this.state = {
      instances: [],
      shouldRender: false
    };
  }

  // Pop API

  /**
   * Removes an instance from the list of active instances.
   *
   * @param {String} id
   */
  remove(id) {
    this.setState({
      instances: this.state.instances.filter(instance => instance.id !== id)
    });
  }

  /**
   * Fetch the list of active instances for this page.
   */
  fetch(callback) {
    pixelpop.fetchInstances(instances => {
      this.setState({ instances }, callback);
    });
  }

  // Editor communication

  /**
   * Send a message to the editor.
   */
  postMessage(message) {
    // Mark all messages as pixelpop messages so we can ignore
    // messages from other sources
    message.pixelpop = true;

    // Send message to editor
    window.top.postMessage(message, process.env.API_URL);
  }

  /**
   * Handle messages from the editor
   */
  onMessage(message) {
    // Ignore messages that are sent from other sources
    if (!message.data || !message.data.pixelpop) {
      return;
    }

    // Update action
    // Update the current array of instances that should be visible.
    // @param instances array
    if (message.data.action === "update") {
      const instances = ((message || []).data || []).instances || [];
      const requiresRemount = this.messageRequiresRemount(instances);
      const newState = { instances };

      if (requiresRemount) {
        newState.shouldRender = false;
      }

      this.setState(newState, () => {
        if (requiresRemount) {
          this.setState({ shouldRender: true });
        }
      });
    }
  }

  /**
   * Returns true if the message requires new assets to be loaded and a fresh
   * remount of the Pop component.
   *
   * This allows us to load new assets only when needed.
   *
   * Note: The centerVertical API only fires when this returns `true`.
   */
  messageRequiresRemount(instances) {
    const hashData = [];

    // To know if the new instances data requires a remount, we build a hash
    // of the data fields that affect layout, and compare it to the last hash
    instances.forEach(instance =>
      hashData.push(
        instance.font,
        instance.imageUrl,
        instance.intent,
        instance.shape,
        instance.size,
        instance.theme
      )
    );

    const hash = hashData.join(",");
    const previousHash = this.previousInstancesHash;
    this.previousInstancesHash = hash;

    return hash !== previousHash;
  }

  bindEditorListener() {
    this._boundOnMessage = this.onMessage.bind(this);
    // Listen for messages from the editor
    window.addEventListener("message", this._boundOnMessage, true);

    // Notify editor that we are ready
    this.postMessage({
      action: "client-ready"
    });
  }

  // Life-cycle methods

  componentWillMount() {
    // Clean up any elements on the page that might interfere
    pixelpop.canvas.wipe(false);

    // Ignore fetching if we are in the editor--the editor will send
    // us instance data when it's ready
    if (!pageMode.isEditor) {
      this.fetch();
    }
  }

  componentDidMount() {
    if (!pageMode.isEditor) {
      return this.setState({ shouldRender: true });
    }

    // set up postmessage listener only if this is the full editor bundle
    // otherwise inject the full editor bundle
    if (process.env.IS_EDITOR_BUNDLE) {
      this.bindEditorListener();
    } else {
      pixelpop.scripts.insert(process.env.EDITOR_SCRIPT_URL);
    }
  }

  componentWillUnmount() {
    // Stop listening for messages from the editor
    if (this._boundOnMessage) {
      window.removeEventListener("message", this._boundOnMessage, true);
      this._boundOnMessage = null;
    }
  }

  render() {
    // skip rendering
    // - temporarily on change of shape settings to force a full remount
    // - if this is the non-editor bundle
    if (!this.state.shouldRender) return null;

    return (
      <div className="pxpop-instances">
        {this.state.instances.map(instance => (
          <Pop
            key={instance.id}
            remove={event => this.remove(instance.id)}
            {...instance}
          />
        ))}
      </div>
    );
  }
}
