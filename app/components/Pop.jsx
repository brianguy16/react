import { h, Component } from 'preact';
import classNames from 'classnames';
import slugify from '../lib/slugify';
import pixelpop from '../lib/api';
import TriggersHandler from '../lib/api/TriggersHandler';
import FrequencyHandler from '../lib/api/FrequencyHandler';
import ClientActionHandler from '../lib/api/ClientActionHandler';
import log from '../lib/devLog';

const raf = (callback) => {
  return window.requestAnimationFrame
    ? window.requestAnimationFrame(callback)
    : callback();
};

// all phases of popup animation
const POP_STATES = {
  HIDDEN: {
    visible: false,
    animatingIn: false,
    animatingOut: false,
  },
  IN: {
    visible: false,
    animatingIn: true,
    animatingOut: false,
  },
  IN_VISIBLE: {
    visible: true,
    animatingIn: true,
    animatingOut: false,
  },
  VISIBLE: {
    visible: true,
    animatingIn: false,
    animatingOut: false,
  },
  OUT: {
    visible: false,
    animatingIn: false,
    animatingOut: true,
  },
}

/**
 * A pixelpop instance.
 *
 * Handles any logic shared between themes and pop types.
 */


export default class Pop extends Component {
  // Default shape settings
  static defaultShapeSettings = {
    scrollLock: false,
    hasOverlay: false,
    hasPadding: false,
    overlayShouldDismiss: false,
    presentDelay: 0,
    dismissDelay: 0,
    centeredVertically: false,
  };

  constructor(props) {
    super(props);

    // Initial state
    this.state = {
      ...POP_STATES.HIDDEN,
      shouldRender: false,
    };

    this.present = this.present.bind(this);

    // actions passed into popups themselves
    this.actions = {
      dismiss: this._registerDismissal.bind(this),
      subscribe: this._registerSubscription.bind(this),
      convert: this._registerConversion.bind(this),
    };

    this.loadTheme(props);
  }

  loadTheme(props) {
    // Pull theme from registry
    const theme = window._pxu.getTheme(props);

    // Allow theme to preprocess props
    this.themeProps = theme.processProps(props);

    // allow themes to insert dynamic styles
    this.themeDynamicStyles = theme.dynamicStyles
      ? theme.dynamicStyles(this.themeProps)
      : null;

    // Get theme components
    this.themeComponents = {
      Shape: window._pxu.getShape(props),
      Intent: window._pxu.getIntent(props),
      Overlay: this.getOverlayForPopup(this.themeProps),
    };

    // Get a theme's shape settings
    this.shapeSettings = {
      ...Pop.defaultShapeSettings,
      ...(this.themeComponents.Shape.shapeSettings || {})
    };

    this.theme = theme;

    this.triggers = new TriggersHandler();
    this.actionHandler = new ClientActionHandler(this.props);
    this.frequencyHandler = new FrequencyHandler(this.props);
  }

  componentDidMount() {
    Promise.all([
      pixelpop.styles.insert(this.theme, this.props),
      this.triggers.listen(this.props),
      this.frequencyHandler.willPresent(),
    ]).then(() => {
      this.setState( { shouldRender: true }, () => {
        raf(this.present);
      });
    }).catch (e => e && log(e));
  }

  componentWillReceiveProps(nextProps) {
    this.loadTheme(nextProps);
  }

  componentWillUnmount() {
    this.shapeTeardown();
  }

  /**
   * Set state only once per animation frame
   */
  rafSetState(newState, callback) {
    return raf(() => this.setState(newState, callback));
  }

  shapeSetup() {
    if (this.shapeSettings.scrollLock) {
      pixelpop.scrollLock.add();
    }

    if (this.shapeSettings.hasPadding) {
      this.padder = this.currentShape.getPadding.bind(this.currentShape);
      pixelpop.padding.add(this.padder);
    }
  }

  shapeTeardown() {
    if (this.shapeSettings.scrollLock) {
      pixelpop.scrollLock.remove();
    }

    if (this.padder) {
      pixelpop.padding.remove(this.padder);
      delete this.padder;
    }

    clearTimeout(this.closeTimer);
  }

  getOverlayForPopup(themeProps) {
    const overlayComponent = themeProps.overlays[themeProps.shape];

    // skip if no overlay is defined
    if (!overlayComponent) return false;

    // return custom overlay
    return overlayComponent;
  }


  // Server actions

  /**
   * Subscribe to newsletter.
   *
   * @param {String} email
   */
  _registerSubscription(email) {
    this.frequencyHandler.registerSubscription(email);

    return this.actionHandler.convert({
      fields: {
        email,
      },
    });
  }

  /**
   * Track a conversion.
   */
  _registerConversion() {
    this.frequencyHandler.registerConversion();
    return this.actionHandler.convert();
  }

  /**
   * Track popup as dismissed.
   *
   * @param {Boolean} persistDismissal - whether or not the action should
   * persist the dismissal to show again or not
   */
  _registerDismissal(options = {persistDismissal: false}) {
    if (!options.persistDismissal) {
      //store dismissal in localstorage
      this.frequencyHandler.registerDismissal();
    }

    // Hide the popup locally
    this.dismiss();
  }

  // Presentation

  /**
   * Show the popup, waiting for any animations if requested.
   */
  present() {
    if (this.state.visible) return;

    // Set up global page state
    this.shapeSetup();

    // trigger animation lifecycle
    this.rafSetState(POP_STATES.IN, () => {
      this.rafSetState(POP_STATES.IN_VISIBLE, () => {
        this.frequencyHandler.registerImpression();
        this.actionHandler.impress();

        // Start a timer so we can toggle off the animating state when
        // the animation has completed
        this.animationTimer = setTimeout(() => {
          this.setState(POP_STATES.VISIBLE);
        }, this.shapeSettings.presentDelay || 0);
      });
    });
  }

  /**
   * Hide the popup, waiting for any animations if requested.
   */
  dismiss() {
    this.setState(POP_STATES.OUT, () => {

      // Start a timer so we remove the element only after it
      // has finished animating
      this.animationTimer = setTimeout(
        () => {
          // Clean up global page state
          this.shapeTeardown();
          this.props.remove(this.props.id);
        },
        this.shapeSettings.dismissDelay || 0
      );

    });
  }

  onKeyDown(event) {
    if (event.keyCode === 27 /*esc*/) {
      event.preventDefault();
      this.dismiss();
    }
  }

  generateClassNames() {
    const slugString = slugify(this.props.font);

    const staticClasses = [
      'pxpop-instance',
      `pxpop-shape-${this.props.shape}`,
      `pxpop-intent-${this.props.intent}`,
      `pxpop-position-${this.props.position}`,
    ];

    return classNames(staticClasses, {
      'pxpop-dismissible': this.props.dismissible,
      'pxpop-has-branding': this.props.showBranding,
      'pxpop-visible': this.state.visible,
      'pxpop-animating-in': this.state.animatingIn,
      'pxpop-animating-out': this.state.animatingOut,
      'pxpop-has-image': this.props.imageUrl,
      [`pxpop-font-${slugString}`]: (typeof this.props.font === 'string'),
      [`pxpop-size-${this.props.size}`]: (typeof this.props.size === 'string'),
      'pxpop-has-shadow': this.props.dropShadow,
    });
  }

  getDynamicStylesComponent() {
    let css = '';

    // Theme dynamic styles
    if (this.themeDynamicStyles) {
      css = css + this.themeDynamicStyles;
    }

    // Custom CSS from editor
    // Make all CSS properties important!
    if (this.props.styles) {
      css = css + this.props.styles
        .replace(/!important/i, '')
        .replace(/;/g, ' !important;');
    }

    return css.length
      ? <style>{css}</style>
      : null;
  }

  render() {

    if (!this.state.shouldRender) return null;

    // Prepare theme components and props
    const { Shape, Intent, Overlay } = this.themeComponents;

    const props = {
      ...this.themeProps,
      actions: this.actions,
    };

    return (
      // Nest our instance inside of a theme class
      // All theme styles will be automatically scoped to this class
      <div className={`pxpop-theme-${this.props.theme}`}>

        {/* Add any dynamic CSS to the page */}
        {this.getDynamicStylesComponent()}

        {/* Add our instance classes to help styling */}
        <div className={this.generateClassNames()}>

          {/* Render Overlay component if requested */}
          {
            Overlay &&
              <Overlay
                shared={props.shared}
                colors={props.colors}
                onClick={props.dismissible ? this.actions.dismiss : null}
                styles={props.styles.overlay || {}}
              />
          }

          {/* Render theme components */}
          <Shape ref={el => this.currentShape = el} {...props}>
            <Intent {...props}/>
            
          </Shape>

        </div>

      </div>
    );
  }
};
