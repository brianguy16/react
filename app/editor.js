/**
 * Our editor bundle basically concatenates all theme/shape/intent scripts for
 * all themes
 *
 * TODO: abstract out the functions in getEntryPoints to reuse them here and
 * make this nice and dynamic based on existing themes.
 */

require('./registry.js');

// Champ

require('./themes/champ/theme.js');

require('./themes/champ/intents/Announcement.jsx');

require('./themes/champ/shapes/Bar.jsx');

