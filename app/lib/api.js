import canvas from './api/canvas';
import PagePadder from './api/PagePadder';
import ScriptRegistry from './api/ScriptRegistry';
import fetchInstances from './api/fetchInstances';
import scrollLock from './api/scrollLock';
import styles from './api/styles';

const scripts = new ScriptRegistry();
const padding = new PagePadder();

export default {
  canvas,
  fetchInstances,
  padding,
  scrollLock,
  scripts,
  styles,
};
