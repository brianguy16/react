import storage from './storage';
import storageKeys from '../localStorageKeys';
import log from '../devLog';
import pageMode from './pageMode';

export default class ClientActionHandler {
  constructor(props) {
    this.impressionUrl = props.impressionUrl;
    this.conversionUrl = props.conversionUrl;

    this.fetchOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
    };
  }

  impress() {
    if (process.env.IS_EDITOR_BUNDLE) return;

    if (pageMode.isPreview) return Promise.resolve();

    if (!this.impressionUrl) {
      return Promise.reject('[pixelpop] could not generate impression');
    }

    const payload = {
      clientId: storage.get(storageKeys.CLIENT_KEY),
    };

    log('sending impression', payload);

    return fetch(this.impressionUrl, {
      ...this.fetchOptions,
      body: JSON.stringify(payload),
    });
  }

  convert(payload = {}) {
    if (process.env.IS_EDITOR_BUNDLE) return;

    if (!this.conversionUrl) {
      return Promise.reject('[pixelpop] could not generate conversion');
    }

    payload.clientId = storage.get(storageKeys.CLIENT_KEY);

    log('sending conversion', payload);

    return fetch(this.conversionUrl, {
      ...this.fetchOptions,
      body: JSON.stringify(payload),
    });
  }
}
