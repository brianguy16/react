import SessionHandler from './SessionHandler'
import storage from './storage';
import storageKeys from '../localStorageKeys';
import log from '../devLog';
import pageMode from './pageMode';

// todo: these constants/keys are being used in other files, we can move them to a storage file and reference them
const CLIENT_KEY = storageKeys.CLIENT_KEY;
const INSTANCES_KEY = 'instances';

export default class FrequencyHandler {
  constructor(instanceProps) {
    storage.set(CLIENT_KEY, instanceProps.clientId);

    this.session = new SessionHandler();
    this.instanceId = instanceProps.id;
    this.instance = instanceProps;
    this.displayOptions = instanceProps.displayOptions;
    this.visitRecord = this._initializeVisitData()[this.instanceId];
  }

  _initializeVisitData() {
    const visitData = this._getVisitData();
    const impression = this._getRecordForInstance();

    if (visitData[this.instanceId]) {
      visitData[this.instanceId].impressions.push(impression);
    } else {
      visitData[this.instanceId] = {
        impressions: [impression],
      };
    }

    return storage.get(INSTANCES_KEY, visitData);
  }

  _getVisitData() {
    return storage.get(INSTANCES_KEY, {});
  }

  _getRecordForInstance() {
    return {
      instanceVersion: 0,
      interactions: {
        loaded: {
          ts: new Date().getTime(),
        }
      },
      url: window.location.url,
      userAgent: navigator.userAgent,
      scriptVersion: 0,
      sessionId: this.session.getSessionId(),
      submitted: false,
    };
  }

  registerImpression() {
    const currentImpression = this._getMostRecentRecord();
    currentImpression.interactions.presented = {
      ts: new Date().getTime(),
    };
    this._updateStorage();
  }

  registerSubscription(emailAddress) {
    const currentImpression = this._getMostRecentRecord();
    currentImpression.interactions.subscribed = {ts: new Date().getTime()};
    currentImpression.interactions.emailSubmitted = {
      ts: new Date().getTime(),
      email: emailAddress
    };
    this._updateStorage();
  }

  registerConversion() {
    const currentImpression = this._getMostRecentRecord();
    currentImpression.interactions.converted = { ts: new Date().getTime() };
    this._updateStorage();
  }

  registerDismissal() {
    const currentImpression = this._getMostRecentRecord();
    currentImpression.interactions.dismissed = {ts: new Date().getTime()};
    this._updateStorage();
  }

  _getMostRecentRecord() {
    const record = this.visitRecord;
    return record.impressions[record.impressions.length - 1];
  }

  _getLastPresentedTimeStamp() {
    const lastPresented = [].concat(this.visitRecord.impressions).reverse();

    for (let i = 0; i < lastPresented.length; ++i) {
      if (lastPresented[i].interactions.presented) {
        return lastPresented[i].interactions.presented.ts;
      }
    }

    return 0;
  }

  _updateStorage() {
    const visitData = this._getVisitData();
    visitData[this.instanceId] = this.visitRecord;
    storage.set(INSTANCES_KEY, visitData);
  }


  //-----------------------------------------
  // Display Schedule
  //-----------------------------------------

  _oncePerNumberVisits() {
    const showPerVisit = this.displayOptions.scheduling.oncePerNumberVisits.value;
    const uniqueSessions = this._getUniqueSessions();
    const sessionsSinceLastPresent = [];

    log(`Showing pop once every ${showPerVisit} visit(s)`);

    // filter session array down to only the sessions since the last one where
    // the popup presented
    for (let i = 0; i < uniqueSessions.length; i++) {
      const currentSession = uniqueSessions[i];
      if (currentSession.presented) break;
      sessionsSinceLastPresent.push(currentSession);
    }

    if (uniqueSessions.length === 1) {
      if (uniqueSessions[0].presented) {
        log('first and only session but shown already during this session');
        return false;
      } else {
        log('first and only session, showing popup');
        return true;
      }
    }

    if (sessionsSinceLastPresent.length >= showPerVisit) {
      log(`${sessionsSinceLastPresent.length} session(s) since popup showed, popup should show`);
      return true;
    }

    log(`${sessionsSinceLastPresent.length} session(s) since popup showed, popup won't show`);
    return false;
  }

  /**
   * Build array of unique sessions from all visit records with a flag for whether
   * a popup presented at any time during that session
   *
   * @return {Array}
   */
  _getUniqueSessions() {
    return []
      .concat(this.visitRecord.impressions)
      .reverse()
      .reduce((sessionArray, currentRecord, index) => {
        const sessionId = currentRecord.sessionId;
        let foundSession = false;

        for (let i = 0; i < sessionArray.length; ++i) {
          if (sessionArray[i].id === sessionId) {

            // if the session has been added but `presented` is false, update
            // session object with presented value from current record
            if (!sessionArray[i].presented) {
              sessionArray[i].presented = !!currentRecord.interactions.presented;
            }

            foundSession = true;
            break;
          }
        }

        if (!foundSession) {
          sessionArray.push({
            id: sessionId,
            presented: !!currentRecord.interactions.presented,
          });
        }

        return sessionArray;
      }, []);
  }

  _showOnceEveryDateTime() {
    const schedule = this.displayOptions.scheduling.timesPerDateSpan;
    const lastPresented = this._getLastPresentedTimeStamp();
    const now = new Date().getTime();
    const timeSinceLastPresent = now - lastPresented;
    const scheduleSpanTs = this._getIntervalStampFromString(schedule.span) * parseInt(schedule.value, 10);

    return timeSinceLastPresent > scheduleSpanTs;
  }

  _getIntervalStampFromString(spanString) {
    const daySpan = 1000 * 60 * 60 * 24;

    switch(spanString) {
      case 'day':
        return daySpan;
        break;
      case 'week':
        return daySpan * 7;
        break;
      case 'month':
        return daySpan * this._getMonthDayCount();
        break;
      default:
        console.warn('[pixelpop] invalid day span string!');
        return 0;
        break;
    }
  }

  _getMonthDayCount() {
    const currentDate = new Date();
    return new Date(currentDate.getFullYear(), currentDate.getMonth(), 0).getDate();
  }

  //-----------------------
  // Stopping Conditions
  //-----------------------
  _isDismissed() {
    const enabled = this.displayOptions.stoppingConditions.afterClosed.enabled;

    if (!enabled) return true;
      for (let i = 0; i < this.visitRecord.impressions.length; ++i) {
        if(this.visitRecord.impressions[i].interactions.dismissed) {
          log('not showing popup: previously dismissed');
          return false;
        }
      }
      return true;
    }

  _isDisplayCount() {
    const timesViewed = this.displayOptions.stoppingConditions.afterTimesViewed;
    if (!timesViewed.enabled) return true;
    let popupDisplayed = 0;

    for (let i = 0; i < this.visitRecord.impressions.length; ++i) {
      if (this.visitRecord.impressions[i].interactions.presented)
        popupDisplayed++;
    }

    if (popupDisplayed < timesViewed.value) {
      return true;
    } else {
      log(`not showing popup: viewed more than ${timesViewed.value} time(s)`);
      return false;
    }
  }

  _isSubmitted() {
    const enabled = this.displayOptions.stoppingConditions.whenInteractedWith.enabled;
    if (!enabled) return true;

    for (let i = 0; i < this.visitRecord.impressions.length; ++i) {
      if (this.visitRecord.impressions[i].interactions.converted || this.visitRecord.impressions[i].interactions.subscribed) {
        log('not showing popup: previously interacted with');
        return false;
      }
    }

    return true;
  }

  _getDisplayRules() {
    const displayOptions = this.displayOptions.scheduling;
    if (displayOptions.oncePerNumberVisits.enabled) return this._oncePerNumberVisits();
    if (displayOptions.timesPerDateSpan.enabled) return this._showOnceEveryDateTime();

    return true;
  }

  // the '~' is weird but - http://www.jstips.co/en/javascript/even-simpler-way-of-using-indexof-as-a-contains-clause/
  _getStoppingConditions() {
    return ~[
      // if any of these return false.. DO NOT SHOW POPUP
      this._isDismissed(),
      this._isDisplayCount(),
      this._isSubmitted()
    ].indexOf(false) ? true : false; // stopping condition returns true for stopping.
  }

  _showInstance() {
    const shouldStopShowing = this._getStoppingConditions();
    return shouldStopShowing ? false : this._getDisplayRules();
  }

  willPresent() {
    if (pageMode.isPreview || pageMode.isEditor) return Promise.resolve();

    if (!this.displayOptions) return Promise.resolve();

    if (this._showInstance()) {
      return Promise.resolve();
    } else {
      return Promise.reject();
    }
  }
}
