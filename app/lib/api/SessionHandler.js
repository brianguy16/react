import guid from './generateGuid';

const DEFAULT_SESSION_KEY = 'pixelpop-session';

export default class SessionHandler {
  constructor() {
    this._initializeSession();
  }

  _get() {
    if (!window.sessionStorage) {
      return null;
    }

    const session = JSON.parse(window.sessionStorage.getItem(DEFAULT_SESSION_KEY));
    if (session) return session;

    return null;
  }

  _set(data) {
    if (!data) {
      return;
    }

    if (!window.sessionStorage) {
      return;
    }

    try {
      window.sessionStorage.setItem(DEFAULT_SESSION_KEY, JSON.stringify(data));

      return data;
    } catch (exception) {
      console.warn('[pixelpop] session was not created/updated!');
    }
  }

  _initializeSession() {
    const currentSession = this._get();
    if (currentSession) {
      const sessionCount = {
        id: currentSession.id,
        pageViews: currentSession.pageViews
      };

      return this._set(sessionCount);
    } else {
      const sessionId = guid.generate();

      return this._set({
        id: sessionId,
        pageViews: 0,
      });
    }
  }

  getViewCount() {
    return this._get().pageViews;
  }

  incrementViewCount() {
    const session = {
      id: this.getSessionId(),
      pageViews: this.getViewCount() + 1
    };

    this._set(session);

    return session.pageViews;
  }

  getSessionId() {
    return this._get().id;
  }
}

