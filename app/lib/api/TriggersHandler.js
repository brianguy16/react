export default class TriggersHandler {
  conditions = [];
  delays = {};

  _setupDelayValues(props) {
    return {
      scroll: props.delayPercentScrolled || 0,
      time: props.delayTimeInSeconds || 0,
      exit: props.delayExitIntentEnabled || false,
    };
  }

  // Time Delay
  // ----------

  _conditionTimeElapsed() {
    const timeInMilliseconds = this.delays.time * 1000;

    return new Promise((resolve, reject) => {
      setTimeout(resolve, timeInMilliseconds);
    });
  }


  // Scroll Delay
  // ------------

  _conditionPercentScrolled() {
    return new Promise((resolve, reject) => {
      const onScroll = () => {
        const percentScrolled = this._getVerticalScrollPercent();
        if (percentScrolled >= this.delays.scroll) {
          window.removeEventListener('scroll', onScroll);
          resolve();
        }
      }

      window.addEventListener('scroll', onScroll);
    });
  }

  _getVerticalScrollPercent() {
    const windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight || 0;
    const yScroll = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop || 0;
    const docHeight = Math.max(
      document.body.scrollHeight || 0,
      document.documentElement.scrollHeight || 0,
      document.body.offsetHeight || 0,
      document.documentElement.offsetHeight || 0,
      document.body.clientHeight || 0,
      document.documentElement.clientHeight || 0
    );
    return (((yScroll + windowHeight) / docHeight) * 100);
  }


  // Exit Intent
  // -----------

  _conditionExitAttempted() {
    return new Promise((resolve, reject) => {
      const onMouseLeave = (event) => {
        if (event.clientY <= 0) {
          document.removeEventListener('mouseout', onMouseLeave);
          resolve();
        }
      }

      document.addEventListener('mouseout', onMouseLeave);
    });
  }


  listen(props) {
    this.delays = this._setupDelayValues(props);

    if (this.delays.time) {
      this.conditions.push(this._conditionTimeElapsed());
    }

    if (this.delays.scroll) {
      this.conditions.push(this._conditionPercentScrolled());
    }

    if (this.delays.exit) {
      this.conditions.push(this._conditionExitAttempted());
    }

    return Promise.all(this.conditions);
  }
}
