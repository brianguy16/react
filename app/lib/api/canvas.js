/**
 * Run a function continually, with a increasing delay.
 *
 * If the watcher function returns `true`, it will stop the running.
 */
function watch(limit, watcher) {
  let delay = 100;
  let iterations = 0;

  const timer = () => {
    if (iterations > limit) return;
    if (watcher()) return;

    delay *= 1.5;
    iterations += 1;

    setTimeout(timer, delay);
  };

  timer();
}

/**
 * Clear elements specifically when in the editor.
 */
function clearInEditor() {
  // Remove Shopify admin bar
  watch(10, () => {
    const element = document.getElementById('admin_bar_iframe');

    if (element) {
      element.outerHTML = '';
      return true;
    }
  });

  // Remove top positioning that the Shopify admin bar adds
  watch(10, () => {
    const elements = document.querySelectorAll('.pxpop-instance');

    for (var i = 0; i < elements.length; i++) {
      const element = elements[i];
      element.removeAttribute('style');
    }
  });
}

export default {
  wipe: (isEditor) => {
    if (isEditor) {
      clearInEditor();
    }
  }
}
