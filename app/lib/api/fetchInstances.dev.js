// Fetching  instances from local filesystem

export default (function() {
  function overrideInstances(data, overrides) {
    const instances = data.instances.map((instance) => {
      return {...instance, ...overrides};
    });

    return {data, ...{instances}};
  }

  function paramsToJson(params) {
    if (!params) return {};

    params = params
      .replace(/&/g, '","')
      .replace(/=/g, '":"');

    return JSON.parse(
      `{"${params}"}`,
      function(key, value) {
        return key === "" ? value : decodeURIComponent(value);
      }
    );
  }

  return function(callback) {
    const params = location.search.substring(1);
    const overrides = paramsToJson(params);
    const jsonToFetch = overrides['instances'] || 'bar-and-card';

    let data = require(`json-loader!../../../sample/${jsonToFetch}.json`);
    data = overrideInstances(data, overrides);

    callback(data.instances);
  }
})();
