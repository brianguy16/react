if (process.env.STUB_MODE) {
  module.exports = require('./fetchInstances.dev');
} else {
  module.exports = require('./fetchInstances.prod');
}
