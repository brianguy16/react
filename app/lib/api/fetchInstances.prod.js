import storage from './storage';
import SessionHandler from './SessionHandler'
import storageKeys from '../localStorageKeys';

const CLIENT_KEY = storageKeys.CLIENT_KEY;

export default (function() {
  //todo: ideally we would use the second argument of the get to pass in a generated clientID (from a clientIdService)
  const visitId = storage.get(CLIENT_KEY);
  const sessionHandler = new SessionHandler();
  const payload = {
    pageUrl: window.location.href,
  };

  if (visitId) payload.clientId = visitId;
  payload.pageViews = sessionHandler ? sessionHandler.incrementViewCount() : 0;

  return function(callback) {
    // Fetch instances, including our client session ID so the server can
    // decide which instances were dismissed
    fetch(process.env.API_ENDPOINT_URL, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json',
      }
    })

    // Parse response as JSON
    .then(response => response.json())

    // Grab instance data, fold in instance-wide properties
    .then((data) => {
      const instances = data.instances || [];

      return instances.map(instance => {
        return {...instance, showBranding: data.showBranding, clientId: data.clientId, pageViews: data.pageViews}
      });
    })

    // Send instances to be rendered
    .then(instances => callback(instances))

    // Catch any errors
    .catch(exception => console.warn('[pixelpop] Unable to fetch instances.'));
  }
})();
