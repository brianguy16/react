const isPreview = document.location.href.indexOf('pp_force_visible') >= 1;
const isEditor = window.top !== window;

export default {
  isPreview,
  isEditor,
};
