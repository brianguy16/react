export default {
  defaultKey: 'pxu-pp',
  _cache: null,

  get: function (key, defaultValue) {
    this._initialize();

    if (!this._cache[key]) {
      if (defaultValue !== undefined) {
        return this.set(key, defaultValue);
      }
      return null;
    }

    return this.set(key, this._cache[key]);
  },

  set: function (key, value) {
    this._initialize();
    this._cache[key] = value;
    localStorage.setItem(this.defaultKey, JSON.stringify(this._cache));

    return value;
  },

  _initialize:function(){
    if (!this._cache) {
      this._cache = JSON.parse(localStorage.getItem(this.defaultKey));
      if (!this._cache) this._cache = {};
    }
  }
}
