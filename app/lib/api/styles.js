const resetCss = require('../../styles/reset.scss');

/**
 * Inserting theme-specific styles into the page
 * @param {Object} theme - our theme object, containing all theme JS and font data
 * @param {Object} instanceProps - the props for the specific instance we're rendering
 * @return {Promise}
 */
const styles = function(theme, instanceProps) {
  return Promise.all([
    insertRules(theme, instanceProps),
    insertFonts(theme, instanceProps),
  ]);
};

const insertRules = function(theme, instanceProps) {
  return new Promise((resolve, reject) => {
    const cssId = btoa(instanceProps.theme)

    if (document.getElementById(cssId)) return resolve();

    const css = [resetCss, theme.style];

    const stylesheet = document.createElement('style');
    stylesheet.setAttribute('data-pixelpop-style', '');
    stylesheet.id = cssId;
    stylesheet.onload = resolve;
    stylesheet.onerror = reject;
    stylesheet.innerHTML = css.join('\n')

    document.getElementsByTagName('head')[0].appendChild(stylesheet);

    // IE doesn't fire a `load` event for style tags so let's just fake it
    window.setTimeout(resolve, 10);
  });
};


const insertFonts = function(theme, instanceProps) {
  return new Promise((resolve, reject) => {
    // skip out if no font is specified (graphic theme)
    if (!instanceProps.font) resolve();

    const font = theme.fonts[instanceProps.font];
    const fontId = btoa(instanceProps.font);

    if (!font || document.getElementById(fontId)) return resolve();

    const stylesheet = document.createElement('link');
    stylesheet.setAttribute('data-pixelpop-style', '');
    stylesheet.id = fontId;
    stylesheet.rel = 'stylesheet';
    stylesheet.onload = resolve;
    stylesheet.error = reject;
    stylesheet.href = `https://fonts.googleapis.com/css?family=${font}`;

    document.getElementsByTagName('head')[0].appendChild(stylesheet);
  });
};

// Remove all assets on the page that we previously added
const clearStyles = function() {
  const assets = document.querySelectorAll('[data-pixelpop-style]');

  for (var i = 0; i < assets.length; i++) {
    const asset = assets[i];
    asset.parentElement.removeChild(asset);
  }
}

export default {
  insert: styles,
  clear: clearStyles,
};
