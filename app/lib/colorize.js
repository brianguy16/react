// convert a decimal value to hex
function decimalToHex(decimal) {
  return decimal.toString(16);
}

// convert a hex value to decimal
function hexToDecimal(hex) {
  return parseInt(hex, 16);
}

export function mix(colorA, colorB, weight = 0.5) {
  let colorC = '#';

  // loop through each of the 3 hex pairs—red, green, and blue
  for (let i = 0; i <= 5; i += 2) {
    const v1 = hexToDecimal(colorA.replace('#', '').substr(i, 2));
    const v2 = hexToDecimal(colorB.replace('#', '').substr(i, 2));

    // combine the current pairs from each source color, according to the specified weight
    let val = decimalToHex(Math.floor(v2 + (v1 - v2) * weight));

    // prepend a '0' if val results in a single digit
    while (val.length < 2) {
      val = `0${val}`;
    }

    colorC += val;
  }

  return colorC;
}

// Determine the mix color based on color palette
export function getMixColor(backgroundColor, textColor, accentColor, fallBackColor = '#000000') {
  // threshold is the minimum percentage in difference two hex values should be
  // calculated based off white vs. darkgrey (default placeholder color)
  const threshold = 33;
  const hexArgs = [].slice.call(arguments).map(arg => parseInt(arg.substr(1, arg.length), 16));

  // if the bg and text color are too close
  if (percentageDifference(hexArgs[0], hexArgs[1]) >= 0 && percentageDifference(hexArgs[0], hexArgs[1]) < threshold) {
    // if the bg color and the accent color are too close, fall back
    if (percentageDifference(hexArgs[0], hexArgs[2]) >= 0 && percentageDifference(hexArgs[0], hexArgs[2]) < threshold) {
      return fallBackColor;
    } else {
      return accentColor;
    }
  }

  return textColor;
}

function percentageDifference(numA, numB) {
  return Math.abs(Math.ceil(100 - ((numB/numA)*100)));
}
