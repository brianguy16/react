/**
 * Log to console only if session key is set
 *
 * Pass the log type as the last argument: `log(var1, var2, 'warn')`
 * If last arg isn't a valid log type, defaults to 'log'
 */
export default function log() {
  if (!window._pxu.debugEnabled) return;

  const types = ['log', 'warn', 'error'];
  const args = arguments;
  let type = types[0];

  if (types.indexOf(args[args.length - 1]) > -1) {
    type = args.pop();
  }

  console[type].apply(window, [
    '[pixelpop]',
    ...args,
  ]);
}
