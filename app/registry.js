import log from './lib/devLog';
import { Component } from 'preact';

/**
 * Pixelpop theme/component registry
 * ---------------------------------
 *
 * Enables addition of themes and their components to a global `window._pxu` object
 *
 * So far like this:
 *
 * window._pxu.registerShape('dallas', 'modal', (Component) => {
 *   class Modal extends Component {
 *
 *     // component stuff here
 *
 *     render() {
 *       return <div>stuff</div>
 *     }
 *   }
 *
 *   return Modal;
 * });
 */


/**
 * Add a theme's configuration to the registry
 * @param  {String}   theme        the theme name
 * @param  {Function} getThemeData returns an object of theme config functions/objects
 */
function registerTheme(theme, getThemeData) {
  if (this.themes[theme]) {
    log(`theme "${theme}" registering twice!`, 'warn');
    return;
  };

  this.themes[theme] = getThemeData();
}

/**
 * Pull a theme config from the registry
 * @param  {Object} instanceProps - the instance object returned from the API
 * @return {Object} theme         - an object of theme config functions/objects
 */
function getTheme(instanceProps) {
  const theme = this.themes[instanceProps.theme];

  if (!theme) {
    log(`theme "${instanceProps.theme}" not registered!`, 'warn');
    return;
  }

  return theme;
}

/**
 * Add a shape component to the registry
 * @param  {String}   theme        - the theme name
 * @param  {String}   shape        - the shape (one of modal, card, bar, or
 *                                   takeover, maybe more to come)
 * @param  {Function} getComponent - A callback function that supplies the shape
 *                                   component with preact's Component class, and
 *                                   an object of shared components (TODO/TBD)
 */
function registerShape(theme, shape, getComponent) {
  const shapeKey = `${theme}.${shape}`;

  if (this.shapes[shapeKey]) {
    log(`Shape "${shape}" for theme "${theme}" registering twice!`);
    return;
  }

  this.shapes[shapeKey] = getComponent(Component);
}

/**
 * Pull a shape component from the registry
 * @param  {Object} instanceProps - the instance object returned from the API
 * @return {Function}             - a preact component for the shape
 */
function getShape(instanceProps) {
  const { shape, theme } = instanceProps;
  const ShapeComponent = this.shapes[`${theme}.${shape}`];

  if (!ShapeComponent) {
    log(`shape ${shape} for theme ${theme} not registered!`, 'warn');
    return;
  }

  return ShapeComponent;
}

/**
 * Add an intent component to the registry
 * @param  {String}   theme        - the theme name
 * @param  {String}   shape        - the intent name
 * @param  {Function} getComponent - A callback function that supplies the intent
 *                                   component with preact's Component class, and
 *                                   an object of shared components (TODO/TBD)
 */
function registerIntent(theme, intent, getComponent) {
  const intentKey = `${theme}.${intent}`;

  if (this.intents[intentKey]) {
    log(`Intent "${intent}" for theme "${theme}" registering twice!`, 'warn');
    return;
  }

  this.intents[intentKey] = getComponent(Component);
}


/**
 * Pull an intent component from the registry
 * @param  {Object} instanceProps - the instance object returned from the API
 * @return {Function}             - a preact component for the intent
 */
function getIntent(instanceProps) {
  const { intent, theme } = instanceProps;
  const IntentComponent = this.intents[`${theme}.${intent}`];

  if (!IntentComponent) {
    log(`intent ${intent} for theme ${theme} not registered!`, 'warn');
    return;
  }

  return IntentComponent;
}

// don't overwrite anything else we may have attached here
window._pxu = window._pxu || {};

window._pxu.debugEnabled = localStorage.getItem('pxu-debug') === 'true';

window._pxu.enableDebug = function() {
  localStorage.setItem('pxu-debug', true);

  return 'Debug enabled! Please ave a great day.';
}

// setters
window._pxu.registerTheme = registerTheme;
window._pxu.registerShape = registerShape;
window._pxu.registerIntent = registerIntent;

//getters
window._pxu.getTheme = getTheme;
window._pxu.getShape = getShape;
window._pxu.getIntent = getIntent;

// stores
window._pxu.themes = {};
window._pxu.shapes= {};
window._pxu.intents= {};
