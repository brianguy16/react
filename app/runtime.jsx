import { h, render } from 'preact';
import App from './components/App.jsx';

function getRootElement() {
  let el = document.querySelector('#pixelpop');
  if (el) return el;

  el = document.createElement('div');
  el.setAttribute('id', 'pixelpop');
  document.body.appendChild(el);

  return el;
}

render(<App/>, getRootElement());
