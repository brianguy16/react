import { h } from 'preact';

window._pxu.registerIntent('champ', 'announcement', () => {
  function Announcement(props) {
    const {title, body} = props.content;
    const { TitleLine } = props.shared;

    return (
      <div className="pxpop-content">
        <TitleLine bgColor={props.colors.accentColor}/>
        <div className="pxpop-title">{title}</div>
        <div className="pxpop-body" dangerouslySetInnerHTML={{ __html: body}}/>
      </div>
    );
  }

  return Announcement;
});


