import { h } from 'preact';

window._pxu.registerShape('champ', 'bar', (Component) => {
  class Bar extends Component {
    static shapeSettings = {
      presentDelay: 500,
      dismissDelay: 500,
    };

    render() {
      const { Close, Image } = this.props.shared;

      return (
        <div>
          <div className="pxpop-shape" style={this.props.styles}>
            { this.props.imageUrl && <Image url={ this.props.imageUrl } alt={ this.props.title } /> }
            { this.props.children }
          </div>
          { this.props.dismissible && <Close color={this.props.colors.accentColor} onClick={this.props.actions.dismiss}/> }
        </div>
      );
    }
  }

  return Bar;
});
