import { h } from 'preact';

export default function Close(props) {
  return (
    <a className="pxpop-close" onClick={props.onClick} style={{ color: props.color }}>
      <svg width="15px" height="15px" viewBox="0 0 15 15">
        <polygon points="0.707106781 -8.8817842e-16 14.8492424 14.1421356 14.1421356 14.8492424 1.09912079e-13 0.707106781"></polygon>
        <polygon points="14.8492424 0.707106781 0.707106781 14.8492424 1.11910481e-13 14.1421356 14.1421356 -3.77475828e-15"></polygon>
      </svg>
    </a>
  );
}
