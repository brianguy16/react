import { h } from 'preact';

export default function Image(props) {
  return (
    <div className="pxpop-image" style={{backgroundImage: `url(${ props.url })`}}>
      <img src={ props.url } alt={ props.alt } />
    </div>
  );
}
