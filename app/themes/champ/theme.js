
import Close from './shared/Close';
import Image from './shared/Image';

const style = require('./style.scss');

function processProps(props) {
  const colors = {
    textColor: props.colors[0] || '#ead7ba',
    accentColor: props.colors[1] || '#ffc229',
    backgroundColor: props.colors[2] || '#1b2c40',
  };

  const styles = {
    color: colors.textColor,
    backgroundColor: colors.backgroundColor,

    overlay: {
      backgroundColor: colors.backgroundColor,
    },
  };

  const shared = {
    Close,
    Image,
  };

  const overlays = {
    modal: 'default',
    takeover: 'default',
  };

  return {
    ...props,
    colors,
    overlays,
    styles,
    shared,
  };
}

window._pxu.registerTheme('champ', () => {
  return {
    style,
    processProps,

    fonts: {
      'a': 'Poppins:400,500,700',
      'b': 'Work+Sans:400,500,800',
      'c': 'Rubik:400,700',
      'd': 'Unica+One|Work+Sans:400',
      'e': 'Fredoka+One|Rubik:400'
    }
  };
});
